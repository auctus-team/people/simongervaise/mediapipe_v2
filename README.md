# Mediapipe V2

This git is alternate version of MediaPipe, allow you to change easily the code to show specific points.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

### launch a docker to use mediapipe :
```
git clone https://gitlab.inria.fr/auctus-team/people/simongervaise/mediapipe_v2.git
cd mediapipe_v2
docker build --tag=mediapipe .

xhost local:docker
docker run -it --name mediapipe --env="DISPLAY" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" --device="/dev/video0:/dev/video0" mediapipe:latest
```
### remove useless files to replace them by the git repo

```
cd ..
rm -r mediapipe/
git clone https://gitlab.inria.fr/auctus-team/people/simongervaise/mediapipe_v2.git
```
### install mediapipe library

```
cd mediapipe_v2
source .installation
```

### launch a demo

```
cd mediapipe_v2/mediapipe/test/hand-detection
python3 hand_detection_v2.py
```

## Change the code

In order to select points you want to see on the screen, you can easliy modify it by going in the program /usr/local/lib/python3.8/dist-packages/mediapipe/python/solutions/hand_detection/drawing_utils.py

At line 180, you will be able to modify the list in order to choose the point to show.
