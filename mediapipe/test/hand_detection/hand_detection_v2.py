import cv2
import mediapipe as mp
mp_drawing = mp.hand_detection.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands

mp_hands.Hands(static_image_mode = 4)
print(mp.__file__)

# For static images:
mp_model = mp_hands.Hands(
    static_image_mode=True, # only static images
    max_num_hands=2, # max 2 hands detection
    min_detection_confidence=0.5) # detection confidence

# we are not using tracking confidence as static_image_mode is true.
cap = cv2.VideoCapture(0)

while(cap.isOpened()):
	success, image = cap.read()
	if success == True:
		#image = cv2.flip(image, 1)
		
		cv2.waitKey(10)

		results = mp_model.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
		
		print(results.multi_handedness)
		
		image_height, image_width, c = image.shape # get image shape
		# iterate on all detected hand landmarks
		if results.multi_hand_landmarks:
			for hand_landmarks in results.multi_hand_landmarks:
			      # we can get points using mp_hands
			      print(f'Ring finger tip coordinates: (',
				  f'{hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].x * image_width}, '
				  f'{hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].y * image_height})'
			      )
			for hand_landmarks in results.multi_hand_landmarks:
			    mp_drawing.draw_landmarks(
				image, # image to draw
				hand_landmarks, # model output
				mp_hands.HAND_CONNECTIONS, # hand connections
				mp_drawing_styles.get_default_hand_landmarks_style(),
				mp_drawing_styles.get_default_hand_connections_style())
			
		cv2.imshow("l'image", image)
		if cv2.waitKey(25) & 0xFF == ord('q'):
	      		break
	else :
		break

cap.release()
cv2.destroyAllWindows()
